import React from 'react'
import TodoInput from './components/TodoInput'


class App extends React.Component {
  state = {
    task: "",
    id: 1,
    complete: false
  }
  handleChange = (e) => {
    this.setState({
      task: e.target.value
    })
  }


  handleSubmit = (e) => {
    console.log(this.state.task)
    this.setState({
      task: ""
    });
    e.preventDefault();

  }

  render() {
    return (
      <div>
        <h1>Todos</h1>
        <form onSubmit={this.handleSubmit}>
          <input type="text" value={this.state.task} onChange={this.handleChange} />
          <button > Submit</button>
        </form>

        <TodoInput text={this.state} />
      </div>


    )

  }



}

export default App;