import React from 'react';

const TodoList = ({data}) => {
  console.log('Our props', data);
  return (
    <div className='container'>
      <ul class="collection">
        <li class="collection-item">{data.task}</li>
      </ul>
    </div>
  );
};

export default TodoList;
